### Overview

* review 2 way data binding
* review directives
* review angular structure - modules, controllers, built-in services

* recreate app from last week using boilerplate and display a list of songs for an artist
* create custom service for itunes api
* trigger search based on user input using a form instead of page load
	* create form
	* ng-submit and methods on $scope
	* ng-hide for showing placeholder "Please make a search"
* previous searches
	* pushing data into an array of previous search terms
	* ng-switch statements like Handlebars if blockers
* favoriting songs
	* ng-click
	* passing data from ng-repeat to ng-click function
