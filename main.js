var app = angular.module('itunes', []);

app.controller('SongsSearchController', function(iTunes) {
  var vm = this;

  vm.favorites = [];

  vm.search = function() {
    console.log('searching...', vm.artistSearch);
    vm.loading = true;
    iTunes.search(vm.artistSearch).then(function(songs) {
      vm.songs = songs;
      vm.loading = false;
      vm.artistSearch = '';
    });
  };

  vm.favoriteSong = function(song) {
    vm.favorites.push(song);
  };
});

app.factory('iTunes', function($http) {
  return {
    search: function(artist) {
      var url = 'https://itunes.apple.com/search?term='+ artist +'&callback=JSON_CALLBACK';

      return $http.jsonp(url).then(function(response) {
        console.log(response);
         return response.data.results;
      });
    }
  };
});
